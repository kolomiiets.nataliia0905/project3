import { deleteCard } from "../helpers/api.js"
import { getRightData } from "../helpers/otherFunction.js"
import { rendercardOrInput } from "../helpers/otherFunction.js" 
import { getOneCard } from "../helpers/api.js"
import Form from "../classes/Form.js"
import Modal from "../classes/Modal.js"
import { update } from "../helpers/api.js"
import { checkEmptyColection } from "../helpers/otherFunction.js"
export class Visit {
  constructor (id, doctor, fullName, purpose, description, urgency) {
      this.id = id
      this.doctor = doctor
      this.fullName = fullName
      this.purpose = purpose
      this.description = description
      this.urgency = urgency
  }
  deleteCard(){
console.log("test");
      const parentBlock = this.closest(".myCard")
      deleteCard(parentBlock.id).then((result) => {
          if(result.ok){
              parentBlock.remove()
              checkEmptyColection()
          } else {
              throw new Error("Відсутній id")
          }
      }).catch((error) => console.error(error))


  }
  editCard(){
    const form =new Form();
    const modal =new Modal(form.renderEditForm(),"Редагувати");
    modal.render();
    const formContent = document.getElementById("form-content");
    const formEdit = document.getElementById("editForm")
    const parentBlock = this.closest(".myCard")
    getOneCard(parentBlock.id).then(card => {return card.json()}).then(data  => {formContent.insertAdjacentHTML("beforeend", `${rendercardOrInput(data.doctor, "renderInputs", data)}<button type="submit" class="btn w-100 btn-primary">Редагувати</button>`);
    formEdit.onsubmit= async (e) => {
        e.preventDefault();
        const formData = new FormData(formEdit)
        const formObj = {}
        for (const [key, value] of formData) {
            formObj [key] = value;
        }
        formObj.id = data.id
        formObj.doctor = data.doctor
        update(formObj).then(res => {
            console.log(res);
                if (res) {
                    const delCard  = document.getElementById (formObj.id)
                    delCard.remove()
                    rendercardOrInput(formObj.doctor,"renderCard",formObj)
                    modal.delete();

                } else {
                    throw new Error("Не удалось создать")
                }

            }
        ) .catch((e)=> {
            console.log(e.message)})

    }})
    
  }
  renderCard(content){
      const root = document.getElementById("root")
      root.insertAdjacentHTML("beforeend",
      `<div class = "myCard" id="${this.id}">
          <div class = "display">
              <h3 class = "card_svg"><svg class = "white" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 576 512" width = "35" height = "35"><!--! Font Awesome Pro 6.4.0 by @fontawesome - https://fontawesome.com License - https://fontawesome.com/license (Commercial License) Copyright 2023 Fonticons, Inc. --><path d="M256 0h64c17.7 0 32 14.3 32 32V96c0 17.7-14.3 32-32 32H256c-17.7 0-32-14.3-32-32V32c0-17.7 14.3-32 32-32zM64 64H192v48c0 26.5 21.5 48 48 48h96c26.5 0 48-21.5 48-48V64H512c35.3 0 64 28.7 64 64V448c0 35.3-28.7 64-64 64H64c-35.3 0-64-28.7-64-64V128C0 92.7 28.7 64 64 64zM176 437.3c0 5.9 4.8 10.7 10.7 10.7H389.3c5.9 0 10.7-4.8 10.7-10.7c0-29.5-23.9-53.3-53.3-53.3H229.3c-29.5 0-53.3 23.9-53.3 53.3zM288 352a64 64 0 1 0 0-128 64 64 0 1 0 0 128z"/></svg></h3>
              <button class="delete"><svg class = "white" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512" width = "15" height = "15"><!--! Font Awesome Pro 6.4.0 by @fontawesome - https://fontawesome.com License - https://fontawesome.com/license (Commercial License) Copyright 2023 Fonticons, Inc. --><path d="M170.5 51.6L151.5 80h145l-19-28.4c-1.5-2.2-4-3.6-6.7-3.6H177.1c-2.7 0-5.2 1.3-6.7 3.6zm147-26.6L354.2 80H368h48 8c13.3 0 24 10.7 24 24s-10.7 24-24 24h-8V432c0 44.2-35.8 80-80 80H112c-44.2 0-80-35.8-80-80V128H24c-13.3 0-24-10.7-24-24S10.7 80 24 80h8H80 93.8l36.7-55.1C140.9 9.4 158.4 0 177.1 0h93.7c18.7 0 36.2 9.4 46.6 24.9zM80 128V432c0 17.7 14.3 32 32 32H336c17.7 0 32-14.3 32-32V128H80zm80 64V400c0 8.8-7.2 16-16 16s-16-7.2-16-16V192c0-8.8 7.2-16 16-16s16 7.2 16 16zm80 0V400c0 8.8-7.2 16-16 16s-16-7.2-16-16V192c0-8.8 7.2-16 16-16s16 7.2 16 16zm80 0V400c0 8.8-7.2 16-16 16s-16-7.2-16-16V192c0-8.8 7.2-16 16-16s16 7.2 16 16z"/></svg></button>
              <button class="edit"><svg class = "white" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 640 512" width = "15" height = "15"><!--! Font Awesome Pro 6.4.0 by @fontawesome - https://fontawesome.com License - https://fontawesome.com/license (Commercial License) Copyright 2023 Fonticons, Inc. --><path d="M224 256A128 128 0 1 0 224 0a128 128 0 1 0 0 256zm-45.7 48C79.8 304 0 383.8 0 482.3C0 498.7 13.3 512 29.7 512H322.8c-3.1-8.8-3.7-18.4-1.4-27.8l15-60.1c2.8-11.3 8.6-21.5 16.8-29.7l40.3-40.3c-32.1-31-75.7-50.1-123.9-50.1H178.3zm435.5-68.3c-15.6-15.6-40.9-15.6-56.6 0l-29.4 29.4 71 71 29.4-29.4c15.6-15.6 15.6-40.9 0-56.6l-14.4-14.4zM375.9 417c-4.1 4.1-7 9.2-8.4 14.9l-15 60.1c-1.4 5.5 .2 11.2 4.2 15.2s9.7 5.6 15.2 4.2l60.1-15c5.6-1.4 10.8-4.3 14.9-8.4L576.1 358.7l-71-71L375.9 417z"/></svg></button>
          </div>
          <div class = "info">
              <div class = "info_content">
                  <h3 class = "text">ФІО :</h3>
                  <p>${this.fullName}</p>
              </div>
              <div class = "info_content">
                  <h3 class = "text">Лікар :</h3>
                  <p>${this.doctor}</p>
              </div>
              <button class="show-more">Показати більше</button>
              <div class ="card-content hide">
              <div class = "info_content">
                  <h3 class = "text">Терміновість :</h3>
                  <p>${this.urgency}</p>
              </div>
              <div class = "info_content">
                  <h3 class = "text">Опис візиту :</h3>
                  <p>${this.description}</p>
              </div>
              ${content}</div>
          </div>
      </div>`)

const card = document.getElementById(`${this.id}`)
const edit = card.querySelector(".edit")
const button = card.querySelector(".delete")
const btnShow = card.querySelector('.show-more')
button.onclick  = this.deleteCard
btnShow.onclick = this.showMore
edit.onclick = this.editCard
  }

  showMore() {
      const hideContent = this.nextElementSibling
      hideContent.classList.toggle("hide")
      if(hideContent.classList.contains("hide")){
          this.innerText = "Показати більше"
      } else {
          this.innerText = "Приховати"
      }
  }
  renderInputs () {
    return  `<div class="mb-3">
<label  class="form-label">Цель визита</label>
<input name="purpose" type="text" class="form-control" placeholder="Введите цель визита" value = "${getRightData (this.purpose)}">
</div>
<div class="form-floating">

<textarea name="description" value = "${getRightData (this.description)}
class="form-control" placeholder="Пишите описание здесь"></textarea>

</div>
<select  value = "${getRightData (this.urgency)}" name="urgency" class="form-select">
<option selected>Срочность</option>
<option value="Ordinary">Обычная</option>
<option value="Priority">Приоритетная</option>
<option value="Urgent">Неотложная</option>
</select>
<div class="mb-3">
<label  class="form-label">ФИО</label>
<input type="text" name="fullName" value = "${getRightData (this.fullName)}" class="form-control" placeholder="Введите ФИО">
</div>`
  }

}
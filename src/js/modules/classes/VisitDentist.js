import {Visit} from "./Visit.js";
import { getRightData } from "../helpers/otherFunction.js";

export class VisitDentist extends Visit {
    constructor (id, doctor, fullName, purpose, description, urgency, dateLastVisit) {
        super(id, doctor, fullName,purpose, description, urgency)
      this.dateLastVisit=dateLastVisit
    }
    renderCard() {
        const dentistContent =
            `<div class = "info_content">
        <h3 class = "text">Дата последнего визита :</h3>
        <p>${this.dateLastVisit}</p>
        </div>
       
        
        </div>`
        super.renderCard(dentistContent)
    }

    renderInputs () {
      return  `${super.renderInputs()}
        
        
        
        
<div class="mb-3">
  <label  class="form-label">Дата последнего посещения</label>
  <input name="dateLastVisit" type="text" class="form-control" placeholder="Введите дату последнего посещения" value="${getRightData (this.dateLastVisit)}">
</div> `




    }
}
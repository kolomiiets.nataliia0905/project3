const CARDS_URL = "https://ajax.test-danit.com/api/v2/cards"
function getHeaders () {
   const token =  localStorage.getItem("token");
    return {
        'Content-Type': 'application/json',
            'Authorization': `Bearer ${token}`
    }
}
export const getToken = (userEmail, userPassword) => {
   return  fetch("https://ajax.test-danit.com/api/v2/cards/login", {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({ email: userEmail, password: userPassword })
    })
        .then(response => response.text())
     


}
 export const getCards = () => {
    return  fetch(CARDS_URL, {
        method: 'GET',
        headers: getHeaders (),
    })
        .then(response => response.json())



}
export const createObj = (obj) => {

  return  fetch(CARDS_URL, {
        method: 'POST',
        headers:getHeaders(),
        body: JSON.stringify(obj)
    })
        .then(response => response.json())

}
export const deleteCard = (id) => {

    return fetch(`https://ajax.test-danit.com/api/v2/cards/${id}`, {
        method: 'DELETE',
        headers: getHeaders(),
    })
}

export const getOneCard = (id) => {
    return fetch(`https://ajax.test-danit.com/api/v2/cards/${id}`, {
        method: "GET",
        headers: getHeaders(),
    })
}

export const update = (el) => {
    return  fetch(`https://ajax.test-danit.com/api/v2/cards/${el.id}`, {
        method: 'PUT',
        headers:getHeaders(),
        body: JSON.stringify(el)
    })
        .then(response => response.json())
}
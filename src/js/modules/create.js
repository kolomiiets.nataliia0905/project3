import Form from "./classes/Form.js";
import Modal from "./classes/Modal.js";
import {Visit} from "./classes/Visit.js";
import {VisitDentist} from "./classes/VisitDentist.js";
import {VisitCardiologist} from "./classes/VisitCardiologist.js";
import {VisitTherapist} from "./classes/VisitTherapist.js";
import {createObj} from "./helpers/api.js";
import {rendercardOrInput} from "./helpers/otherFunction.js";
import { checkEmptyColection } from "./helpers/otherFunction.js";

const create = document.getElementById("creatBtn");

create.addEventListener("click", () => {
    const form =new Form();
    const modal =new Modal(form.renderCreatForm(),"Создать");
    modal.render();
    const selectDoctor = document.getElementById("selectDoctor");
    selectDoctor.addEventListener("change", () => {
        const formContent = document.getElementById("form-content");
        const formCreate = document.getElementById("creatForm")

        formContent.innerHTML = "";


        formCreate.onsubmit= async (e) => {
            e.preventDefault();
            console.log(formCreate)
            const data = new FormData(formCreate)
            const formObj = {}
            for (const [key, value] of data) {
                formObj [key] = value;
            }
            createObj(formObj).then(res => {
                    if (typeof res==="object") {
                        console.log(res)
                        rendercardOrInput(res.doctor,"renderCard",res)
                        modal.delete();
                        checkEmptyColection()
                    } else {
                        throw new Error("Не удалось создать")
                    }

                }
            ) .catch((e)=> {
                console.log(e.message)})

        }

        formContent.insertAdjacentHTML("beforeend", `${rendercardOrInput(selectDoctor.value, "renderInputs")}<button type="submit" class="btn w-100 btn-primary">Создать</button>`)

    })
})
